<?php

namespace App\Http;

use Illuminate\Support\Facades\DB;

trait RecipeTraits
{

    // check if a recipe exists
    private function validateRecipe($recipe)
    {
        $isExist = DB::table('recipes')->select('id')->where('id', $recipe)->first();

        if ($isExist == null)
            return false;
        if ($isExist->id == 0)
            return false;
        return true;
    }

    private function checkCategory($categoryId)
    {
        $isExist = DB::table('categories')->where('id', $categoryId)->first();
        if ($isExist == null)
            return false;
        if ($isExist->id == 0)
            return false;
        return true;
    }


}