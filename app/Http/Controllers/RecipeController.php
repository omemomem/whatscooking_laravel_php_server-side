<?php

namespace App\Http\Controllers;

use App\Http\UtilityTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\type;
use App\Http\RecipeTraits;
use Symfony\Component\Console\Helper\Table;

class RecipeController extends Controller
{
    use UtilityTraits;
    use RecipeTraits;

    // create new recipe
    public function createRecipe($token, $author, $title, $ingredients, $instructions, $difficulty, $category)
    {
        $this->addUserAction($token, Config::get('constants.recipe.create'));

        $userId = $this->getId($token);
        //TODO: check if category exists , verify fields
        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'user does not exist'
            ]);
        }
        if ($this->checkCategory($category))
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'category does not exist'
            ]);

        $status = DB::table('recipes')->updateOrInsert(
            ['user' => $userId, 'author' => $author, 'title' => $title, 'ingredients' => $ingredients, 'instruction' => $instructions
                , 'views' => 0, 'difficulty' => $difficulty, 'category' => $category],
            ['user' => $userId, 'author' => $author, 'title' => $title, 'ingredients' => $ingredients, 'instruction' => $instructions
                , 'views' => 0, 'difficulty' => $difficulty, 'category' => $category, 'visible' => true]
        );
        if ($status)
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'recipe was added successfully'
            ]);
        return response()->json([
            'status' => Config::get('constants.status.success'),
            'title' => 'recipe re-added'
        ]);
    }

    // update a recipe
    public function updateRecipe($token, $recipe, $title, $ingredients, $instructions, $difficulty)
    {
        $this->addUserAction($token, Config::get('constants.recipe.update'));

        $userId = $this->getId($token);

        $queryConditions = ['id' => $recipe, 'user' => $userId];

        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'user does not exist'
            ]);
        }
        $status = DB::table('recipes')
            ->where($queryConditions)
            ->update(
                ['user' => $userId, 'title' => $title, 'ingredients' => $ingredients, 'instruction' => $instructions
                    , 'views' => 0, 'difficulty' => $difficulty]
            );

        if ($status)
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'recipe was updated successfully'
            ]);
        return response()->json([
            'status' => Config::get('constants.status.success'),
            'title' => 'no changes made to the recipe'
        ]);

    }

    // delete a recipe
    public function deleteRecipe($token, $recipe)
    {
        $this->addUserAction($token, Config::get('constants.recipe.delete'));

        $userId = $this->getId($token);
        $queryConditions = ['id' => $recipe, 'user' => $userId];

        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'user does not exist'
            ]);
        }
        $isOwnRecipe = DB::table('recipes')
            ->select('id')
            ->where($queryConditions)->first();

        if (!($isOwnRecipe)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'recipe is not owned by this user'
            ]);
        }
        DB::table('recipes')
            ->where($queryConditions)
            ->update(['visible' => false]);

        return response()->json([
            'status' => Config::get('constants.status.success'),
            'title' => 'recipe was deleted successfully'
        ]);
    }

    // get own users created recipes
    public function getOwnRecipes($token)
    {
        $this->addUserAction($token, Config::get('constants.recipe.own'));

        $userId = $this->getId($token);
        $recipe = DB::table('recipes')
            ->select('author', 'title',
                'ingredients', 'instruction', 'views')
            ->where('user', $userId)->get();

        if ($recipe->isEmpty())
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'no recipes were created by this user'
            ]);

        return response()->json($recipe);
    }

    // get own users created but deleted recipes
    public function getOwnDeletedRecipes($token)
    {
        $this->addUserAction($token, Config::get('constants.recipe.own_deleted'));

        $userId = $this->getId($token);

        $queryConditions = ['user' => $userId, 'visible' => false];

        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.Failure'),
                'title' => 'user does not exist'
            ]);
        }

        $recipe = DB::table('recipes')
            ->select('author', 'title',
                'ingredients', 'instruction', 'views')
            ->where($queryConditions)->get();

        if ($recipe->isEmpty())
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'no recipes were deleted by this user'
            ]);

        return response()->json($recipe);
    }

    // get all existing categories with their id
    public function getCategories()
    {
        $categories = DB::table('categories')->select('id', 'name')->get();
        return $categories;
    }

    // get a specific recipe with given recipe number [unique number]
    public function getSingleRecipe($token, $recipeId)
    {
        $this->addUserAction($token, Config::get('constants.recipe.get_one'));
        $userId = $this->getId($token);
        $recipe = DB::table('recipes')->select('id', 'user', 'author', 'title',
            'ingredients', 'instruction', 'views')->where('id', $recipeId)->get();

        if ($recipe->isEmpty() || !($userId))
            return response()->json([
                'isAuthor' => false,
                'recipe' => null
            ]);

        // check if requested recipe was created by user
        if ($recipe[0]->user == $userId)
            return response()->json([
                'isAuthor' => true,
                'recipe' => $recipe
            ]);
        else {
            // update view count - only if user is not the author TODO: get IP and monitor views + get upvotes
            DB::table('recipes')
                ->where('id', $recipeId)
                ->update(['views' => ($recipe[0]->views) + 1]);

            return response()->json([
                'isAuthor' => false,
                'recipe' => $recipe
            ]);
        }
    }

    // get all recipes from a specific category, cursor and offset paginated approach
    public function getAllByCategory($token, $category, $page, $timestamp)
    {
        $this->addUserAction($token, Config::get('constants.recipe.get_all'));

        $userId = $this->getId($token);

        try {
            // pages start at 1 arrays at 0, sync page and array choice
            $page--;
            // limit of recipes per page
            $itemsLimit = (int)Config::get('constants.items_limit');
            // offset value calculated with limit and the number of page given
            $offset = $itemsLimit * $page;
        } catch (\Exception $e) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => $e->getMessage()
            ]);
        }
        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'user does not exist'
            ]);
        }
        if ($this->checkCategory($category) == false)
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'category does not exist'
            ]);
        $queryConditions = ['category' => $category, 'visible' => true];

        // TODO: modify orderBy and search parameters
        // get recipes only from before the start of the session and offset according to $page
        $recipes = DB::table('recipes')
            ->select('id', 'user', 'author', 'title',
                'ingredients', 'instruction', 'views')
            ->whereDate('updated_at', '<', $timestamp)
            ->where($queryConditions)
            ->orderByDesc('views')
            ->offset($offset)->limit($itemsLimit)->get();

        return $recipes;
    }

    // vote for a recipe - upvote (like) or downvote (dislike) a recipe
    public function voteRecipe($token, $recipe, $vote)
    {
        $this->addUserAction($token, Config::get('constants.recipe.vote'));

        $userId = $this->getId($token);
        // convert the var into boolean type
        $vote = $this->toBool($vote);
        $matchConditions = ['user' => $userId, 'recipe' => $recipe];

        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.Failure'),
                'title' => 'user does not exist'
            ]);
        }
        if ($this->validateRecipe($recipe) == null) {
            return response()->json([
                'status' => Config::get('constants.status.Failure'),
                'title' => 'recipe does not exist'
            ]);
        }
        $status = DB::table('feedback')->updateOrInsert([$matchConditions], ['user' => $userId, 'recipe' => $recipe, 'upvote' => $vote]);

        if ($status)
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'recipe was voted successfully'
            ]);
        if ($status)
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'recipe was already voted'
            ]);
    }


}
