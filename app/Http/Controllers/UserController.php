<?php

namespace App\Http\Controllers;

use App\Http\UtilityTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LogController;

class UserController extends Controller
{
    use UtilityTraits;

    // create a new user, Token served as password and identifier.
    public function create($description)
    {
        $token = str_random(32);
        $verifyToken = DB::table('users')->select('token')->where('token', $token);

        // generate a random token and check if exists
        while ($verifyToken == $token) {
            $token = str_random(32);
            $verifyToken = DB::table('users')->select('token')->where('token', $token);
        }

        // TODO: hash user token in db
        DB::table('users')->insertgetId(
            ['token' => $token, 'is_blocked' => false, 'is_admin' => false, 'description' => $description]
        );

        $this->addUserAction($token, Config::get('constants.user.create'));

        return response()->json([
            'status' => Config::get('constants.status.success'),
            'title' => 'success',
            'token' => $token
        ]);
    }

    // update user description
    public function updateDescription($token, $description)
    {
        $this->addUserAction($token, Config::get('constants.user.update'));

        $userId = $this->getId($token);

        if ($userId != 0) {
            DB::table('users')->where(['token' => $token])->update(['description' => $description]);
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'success'
            ]);
        } else {
            return response()->json([
                'errors' => ['status:' => Config::get('constants.status.failure'), 'title' => 'failed to validate user']
            ]);
        }
    }

    // block a specific user only by admin user
    public function blockUser($admin, $user)
    {
        $this->addUserAction($admin, Config::get('constants.user.block'));

        $isAdmin = DB::table('users')->where([
            ['token', '=', $admin],
            ['is_admin', '=', true],
        ])->exists();

        if ($isAdmin) {
            DB::table('users')->where(['token' => $user])->update(['is_blocked' => true]);
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'success'
            ]);
        } else
            return response()->json([
                'errors' => ['status:' => Config::get('constants.status.failure'), 'title' => 'failed to validate user']
            ]);
    }

}
