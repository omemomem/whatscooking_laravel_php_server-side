<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\UtilityTraits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\type;
use Symfony\Component\Console\Helper\Table;

class SavedRecipeController extends Controller
{
    use UtilityTraits;

    // add a given recipe to the users saved recipe list
    public function saveRecipe($token, $recipe)
    {
        $this->addUserAction($token, Config::get('constants.saved.save_list_add'));

        $userId = $this->getId($token);

        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'user does not exist'
            ]);
        }

        $recipe = DB::table('recipes')
            ->select('id')
            ->where('id', $recipe)->get();

        if ($recipe->isEmpty())
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'recipe does not exist'
            ]);

        $status = DB::table('saved')->updateOrInsert(
            ['user' => $userId, 'recipe' => $recipe[0]->id],
            ['user' => $userId, 'recipe' => $recipe[0]->id, 'visible' => True]
        );

        if ($status)
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'success'
            ]);
        return response()->json([
            'status' => Config::get('constants.status.failure'),
            'title' => 'recipe is already saved'
        ]);
    }

    // remove a recipe from the saved recipes list
    public function removeRecipe($token, $recipe)
    {
        $this->addUserAction($token, Config::get('constants.saved.save_list_remove'));

        $userId = $this->getId($token);

        if (!($userId)) {
            return response()->json([
                'status' => Config::get('constants.status.failure'),
                'title' => 'user does not exist'
            ]);
        }

        $status = DB::table('saved')->where(array('user' => $userId, 'recipe' => $recipe))->update(array('visible' => false));

        if ($status)
            return response()->json([
                'status' => Config::get('constants.status.success'),
                'title' => 'recipe was deleted'
            ]);
        return response()->json([
            'status' => Config::get('constants.status.success'),
            'title' => 'recipe does not exist'
        ]);
    }

    // get saved recipes of a given user
    public function getSavedRecipe()
    {
        // TODO: also check if given recipes are visible, if not tell user it was deleted (?)
    }
}
