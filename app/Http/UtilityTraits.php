<?php
/**
 * Created by PhpStorm.
 * User: omermeirovich
 * Date: 31/01/2019
 * Time: 17:59
 */

namespace App\Http;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

trait UtilityTraits
{
    // get id from token of user
    private function getId($token)
    {
        $userId = DB::table('users')->where('token', '=', $token)->select('id')->first();
        // If user was not found
        if ($userId == null)
            return 0;
        return $userId->id;
    }

    // add action to user_log
    private function addUserAction($token, $action)
    {
        //TODO: hash user IP in db!
        $userId = $this->getId($token);

        if ($userId != null) {
            DB::table('user_action_log')->insert(
                ['user' => $userId, 'action' => $action, 'ip' => request()->ip()]
            );
        } else
            DB::table('user_action_log')->insert(
                ['user' => $userId, 'action' => $action . Config::get('constants.errors.unauthorized'),
                    'ip' => request()->ip()]
            );
    }


    // get id of user from its token
    private function getIdFromToken($token)
    {
        return DB::table('note_user')->select('users')->where('token', '=', $token)->first();
    }

    // convert a string used to determine if upvote or downvote to boolean
    function toBool($var)
    {
        if (!is_string($var)) return (bool)$var;
        switch (strtolower($var)) {
            case '1':
            case 'true':
            case 'upvote':
            case 'like':
                return true;
            default:
                return false;
        }
    }
}