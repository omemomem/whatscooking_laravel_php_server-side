<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('user/create/{description}', 'UserController@create');

Route::get('user/update/{token}/{description?}', 'UserController@updateDescription');

Route::get('user/block/{admin}/{user}', 'UserController@blockUser');

Route::get('recipe/create/{token}/{author}/{title}/{ingredients}/{instructions}/{difficulty}/{category}',
    'RecipeController@createRecipe');

Route::get('recipe/update/{token}/{recipe}/{title}/{ingredients}/{instructions}/{difficulty}', 'RecipeController@updateRecipe');

Route::get('recipe/delete/{token}/{recipe}', 'RecipeController@deleteRecipe');

Route::get('recipe/getOne/{token}/{recipe}', 'RecipeController@getSingleRecipe');

Route::get('recipe/categories', 'RecipeController@getCategories');

Route::get('recipe/getOwn/{token}', 'RecipeController@getOwnRecipes');

Route::get('recipe/getOwnDeleted/{token}', 'RecipeController@getOwnDeletedRecipes');

// get timestamp of the start of the users search, paginated cursor in use and page used for paginated offset
Route::get('recipe/getAllByCategory/{token}/{category}/{page}/{timestamp}', 'RecipeController@getAllByCategory');

Route::get('recipe/like/{token}/{recipe}/{vote}', 'RecipeController@voteRecipe');

Route::get('recipe/save/{token}/{recipe}', 'SavedRecipeController@saveRecipe');


