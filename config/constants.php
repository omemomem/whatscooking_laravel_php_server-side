<?php
/**
 * Created by PhpStorm.
 * User: omermeirovich
 * Date: 01/11/2018
 * Time: 0:13
 */

return [

    'status' => [
        'success' => 1,
        'failure' => 0
    ],

    'errors' => [
        'unauthorized' => '_unauthorized'
    ],

    'user' => [
        'create' => "user_create",
        'update' => "user_update",
        'block' => 'user_block',
    ],
    'recipe' => [
        'create' => 'recipe_create',
        'update' => 'recipe_update',
        'delete_own' => 'recipe_delete_own',
        'own' => 'recipe_own',
        'own_deleted' => 'recipe_own_deleted',
        'get_one' => "recipe_get_one",
        'get_all' => 'recipe_get_all',
        'vote' => 'recipe_vote',
    ],
    'saved' => [
        'save_list_add' => 'save_list_add',
        'save_list_delete' => 'save_list_remove',
    ],
    'items_limit' => 50

];